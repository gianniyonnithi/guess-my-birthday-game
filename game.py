from random import randint

username = input("Hi what is your name?\n")

for guess_number in range(1, 6):
 # Ask username if their birthday is month/year
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess", guess_number, username, "were you born in", month , "/", year, "?")
    answer = input("yes or no? ")

    if answer == "yes" :
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
